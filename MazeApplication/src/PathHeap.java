import java.nio.file.Path;
import java.util.ArrayList;

public abstract class PathHeap implements PathOrder {
	private ArrayList<Path> heap;
	private Cell exit;
	
	public PathHeap(Cell exit) {
		heap = new ArrayList<>();
		this.exit = exit;
	}
	
	public PathHeap(PathHeap src) {
		this(src.exit);
		for (Path p: src.heap) {
			this.heap.add(p);
		}
	

	}
	
	public static int leftOf(int i){
		return -1;
		}
	public static int rightOf(int i) {
		return -1;
		}
	public static int parentOf(int i) {
		return -1;
		}


	public void put(Path p) {
	}

	public Path peek() {
		return heap.get(0);
	}

	public Path remove() {
		return heap.remove(0);
	}

	
	public boolean isEmpty() {
		return heap.size() == 0;
	}

}


