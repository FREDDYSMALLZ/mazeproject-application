import java.util.*;

public class Maze {
	
	private static int[][] Maze;
    private static Node[][] prev;

    private static int sizeArrayX;//Size of the array being two integers (X,Y)
    private static int sizeArrayY;
    private static Node lastNode;
	public static Vector<Node> visited;

    Maze(int[][] Maze, int sizeArrayY, int sizeArrayX) {
        this.Maze = Maze;
        this.sizeArrayX = sizeArrayX;
        this.sizeArrayY = sizeArrayY;

        prev = new Node[sizeArrayX][sizeArrayY];
    }

static class Node{
	private int x;
    private int y;


    Node(int x, int y){
        this.x = x;
        this.y = y;
    }

    int getX(){ //Getter methods for the values of X and Y
        return this.x;
    }

    int getY(){
        return this.y;
    }
   /* public void solve(Node start){
        Stack<Node> Mazestack = new Stack<>();
       
        Mazestack.push(start); // Pushes an item to the maze to start
        Mazestack.push(lastNode);
        Mazestack.pop();
        Mazestack.isEmpty();
        Mazestack.size();
    }
    public void push(){ // pushes an Item to the top of the stack
    	
    }
    public void isEmpty(){ //checks is the stack is empty
    	
    }
    public void pop(){//returns  EmptyStackException if the stack empty
    	 	
    }
    public void clear(){
    	
    }
    public int size(){//Returns the number of components in the vector
		return sizeArrayX *sizeArrayY;
    	
    }
    */
    class DepthFirstSearch { //This is the Depth Search algorithm which finds the path
    	                     // in the maze.

        public Stack<Integer> Stack;
        public DepthFirstSearch() {}
        public char[][] DFS (int size, char[][] Maze) {

            Random myDFS = new Random();
            
            Stack = new Stack<Integer>();
            
            int x = myDFS.nextInt(size);
            
            while (x % 2 == 0)
                x = myDFS.nextInt(size);
            
            int y = myDFS.nextInt(size);
            while (y % 2 == 0)
                y = myDFS.nextInt(size);

            Maze[x][y] = ' ';
            
            int total = (size * size) / 4;
            int visited = 1;
            int random[] = new int[4];
            int totalrandom;

            while (visited < total) {
                totalrandom = 0;
                
                if (x > 1 && Maze[x - 2][y] == 'X')
                    random[totalrandom++] = 1;
                if (x < size - 2 && Maze[x + 2][y] == 'X')
                    random[totalrandom++] = 2;
                if (y > 1 && Maze[x][y - 2] == 'X')
                    random[totalrandom++] = 3;
                if (y < size - 2 && Maze[x][y + 2] == 'X')
                    random[totalrandom++] = 4;

                if (totalrandom > 0) {
                    switch(random[myDFS.nextInt(totalrandom)]) {
                        case 1: Maze[x-2][y] = Maze[x-1][y] = ' ';
                                x -= 2;
                                Stack.push(x * size + y);
                                visited++;
                                break;
                        case 2: Maze[x+2][y] = Maze[x+1][y] = ' ';
                                x += 2;
                                Stack.push(x * size + y);
                                visited++;
                                break;
                        case 3: Maze[x][y-2] = Maze[x][y-1] = ' ';
                                y -= 2;
                                Stack.push(x * size + y);
                                visited++;
                                break;
                        case 4: Maze[x][y+2] = Maze[x][y+1] = ' ';
                                y += 2;
                                Stack.push(x * size + y);
                                visited++;
                                break;
                    }
                }
                else {
                    int vert = (int) Stack.pop();
                    x = vert / size;
                    y = vert % size;
                }
            }
            return Maze;
        }

    }

 static class mazePosition{
	 int i;
	 int j;
		
	    public mazePosition(int i, int j) {
		this.i = i; 
		this.j = j;
	    }
	    public int i() { 
	    	return i;
	    	}

	    public int j() {
	    	return j;
	    	}

	    public void print1() {
		System.out.println("(" + i + "," + j + ")");
	    }
	    public mazePosition north() {
		return new mazePosition(i-1, j);
	    }
	    public mazePosition south() {
		return new mazePosition(i+1, j);
	    }
	    public mazePosition east() {
		return new mazePosition(i, j+1);
	    }
	    public mazePosition west() {
		return new mazePosition(i, j-1);
	    } 
	
    LinkedList<points> Square = new LinkedList<points>();
    
    public class points {
    	private int xCoordinate;
    	private int yCoordinate;
    	private char val;
    	private boolean North;
    	private boolean South;
    	private boolean East;
    	private boolean West;

    	public points(){
    	xCoordinate =0;
    	 yCoordinate =0;
    	 val =' ';
    	 
    	North = true;
    	South = true;
    	East = true;
    	West = true;

    	}
    	public points (int X, int Y){
    	xCoordinate = X;
    	 yCoordinate = Y;
    	 
    	}

    	public void setX(int x){
    	xCoordinate = x;
    	
    	}
    	public void setY(int y){
    	 yCoordinate = y;
    	 
    	}

    	public void setNorth(boolean n){
    	 North = n;
    	 
    	}
    	public void setSouth(boolean s){
    	 South= s;
    	 
    	}
    	public void setEast(boolean e){
    	 East = e;
    	 
    	}

    	public void setWest(boolean w){
    	 West = w;
    	 
    	}

    	public int getX(){
    	 return xCoordinate;

    	}

    	public int getY(){
    	 return yCoordinate;
    	 
    	}
    	public char getVal(){
    	 return val;
    	 
    	}

    	public boolean getNorth(){
    	 return North;
    	 
    	}
    	public boolean getSouth(){
    	 return South;
    	 
    	}

    	public boolean getEast(){
    	 return East;
    	 
    	}
    public boolean getWest(){
    	 return West;
    	 
    	}

    	public String toString1(){
    	 String result = "(" + xCoordinate + ", " +yCoordinate + ")";
    	 return result;
    	 
    	}
    	
    	
    }
    private static class Point {
        int x;
        int y;
        Point parent;

        public Point(int x, int y, Point parent) {
            this.x = x;
            this.y = y;
            this.parent = parent;
            
        }

        public Point getParent() {
            return this.parent;
            
        }

        public String toString() {
            return "x = " + x + " y = " + y;
            
        }
  }
//The BFS algorithm will find the maze path from each point 
  public static Queue<Point> BFSqueue = new LinkedList<Point>();

    public static Point getPathBFS(int x, int y) {

       BFSqueue.add(new Point(x,y, null));

        while(!BFSqueue.isEmpty()) {
            Point p = BFSqueue.remove();

            if (Maze[p.x][p.y] == 9) {
                System.out.println("This is the exit. ");
                return p;
            }

            if(isFree(p.x+1,p.y)) {
                Maze[p.x][p.y] = -1;
                Point nextP = new Point(p.x+1,p.y, p);
                BFSqueue.add(nextP);
            }

            if(isFree(p.x-1,p.y)) {
                Maze[p.x][p.y] = -1;
                Point nextP = new Point(p.x-1,p.y, p);
                BFSqueue.add(nextP);
            }

            if(isFree(p.x,p.y+1)) {
                Maze[p.x][p.y] = -1;
                Point nextP = new Point(p.x,p.y+1, p);
                BFSqueue.add(nextP);
            }

             if(isFree(p.x,p.y-1)) {
                Maze[p.x][p.y] = -1;
                Point nextP = new Point(p.x,p.y-1, p);
                BFSqueue.add(nextP);
            }

        }
        return null;
    }


    public static boolean isFree(int x, int y) {
    	
        if((x >= 0 && x < Maze.length) && (y >= 0 &&
        		y < Maze[x].length) && (Maze[x][y] == 0 || Maze[x][y] == 4)) {
        	
            return true;
        }
        
        return false;
    }

    public static void main(String[] args) {

        Point p = getPathBFS(0,0);

         for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(Maze[i][j]);
            }
            
            System.out.println();
        }

        while(p.getParent() != null) {
        	
            System.out.println(p);
            p = p.getParent();
            
        }

    }

}
    
public static void main(String[] args){
	  
	  int [][] Maze =
          {   {1,1,1,1,1,1,1,1,1,1,1},
              {2,2,1,0,1,0,1,0,0,0,0},
              {1,0,1,0,0,0,1,0,1,1,1},
              {1,0,0,0,1,1,1,0,0,0,0},
              {1,0,1,0,0,0,0,0,1,1,1},
              {1,0,1,0,1,1,1,0,1,0,0},
              {1,0,1,0,1,0,0,0,1,1,1},
              {1,0,1,0,1,1,1,0,1,0,1},
              {1,0,0,0,0,0,0,0,0,1,1},
              {0,1,1,0,1,1,1,1,0,0,4}
              

          };
	  
	  Maze m = new Maze(Maze, 10, 11);
	  m.fillPath();

      for(int i = 0; i < Maze.length; i++){
          for(int j = 0; j < Maze[i].length; j++){
              System.out.print(" " + Maze[i][j] + " ");
          }
          System.out.println();
}
}
}


public void fillPath() {
	if (lastNode == null) {
        System.out.println("This is the existing path in the Maze");
    } else {
        
        for (;;) {
            lastNode = prev[lastNode.getY()][lastNode.getX()];

            if (lastNode == null) {
                break;
            }

            Maze[lastNode.getY()][lastNode.getX()] = 2;

        }
    }
}
}


	 
	  
	  
	







	
		
	



	
	


	


