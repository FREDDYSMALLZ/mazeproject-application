import java.nio.file.Path;

public interface PathOrder {
	public void put(Path p);
	public Path peek();
	public boolean isEmpty();
	public Path remove();

}
